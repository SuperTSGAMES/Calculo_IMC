#include<stdio.h>

int main(){
    
  float altura, res, peso;

  printf("Digite seu Peso: ");
  scanf("%f", &peso);

  printf("Digite sua Altura: ");
  scanf("%f", &altura);
  
  res = peso / (altura * altura);

  if (res <=18.5){
    printf("Sua massa Corporal e de: %.2f!\nVoce esta Abaixo do Peso", res);

  }else if(res <= 24.9){
    printf("Sua massa Corporal e de: %.2f!\nVoce esta com Peso Nomal", res);

  }else if (res <=29.9){
    printf("Sua massa Corporal e de: %.2f!\nVoce esta com Sobre Peso", res);
    
  }else if (res <=34.9){
    printf("Sua massa Corporal e de: %.2f!\nVoce esta com Obesidade Classe 1", res);
  
  }else if (res <=39.9)
    printf("Sua massa Corporal e de: %.2f!\nVoce esta com Obesidade Classe 2", res);

  else if (res >= 40){
    printf("Sua massa Corporal e de: %.2f!\nVoce esta com Obesidade Classe 3", res);
    
   }
 
    return 0;
}
